package org.hou.stocktank.platform.stockBase.shell;

import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
public class BasicCommands {
    public final static String ID = "CONSOLE";

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @ShellMethod("echo!")
    public String echo(@ShellOption String input) {
        return input;
    }

    @ShellMethod("Sends fake event")
    public String event(
            @ShellOption String sender,
            @ShellOption String target,
            @ShellOption String type,
            @ShellOption(defaultValue = "") String value) {
        applicationEventPublisher.publishEvent(new StockBaseEvent(this, sender, target, StockBaseEvent.Type.valueOf(type), value));
        return "event has been sent.";
    }

}
