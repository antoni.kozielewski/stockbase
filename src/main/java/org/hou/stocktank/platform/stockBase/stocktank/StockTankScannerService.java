package org.hou.stocktank.platform.stockBase.stocktank;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.base.StrategyFactory;
import org.hou.stocktank.base.json.StrategyFactoryJsonizer;
import org.hou.stocktank.criteria.AvgTransactionTimeCriterion;
import org.hou.stocktank.platform.stockBase.stocktank.storage.*;
import org.hou.stocktank.utils.MBankProfitCriterion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.ta4j.core.*;
import org.ta4j.core.analysis.criteria.BuyAndHoldCriterion;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@ConfigurationProperties(prefix = "stocktank")
public class StockTankScannerService {
    private static final Logger logger = LoggerFactory.getLogger(StockTankScannerService.class);
    private List<String> stocks = new ArrayList<>();
    private List<String> strategies = new ArrayList<>();
    private List<String> analyzers = new ArrayList<>();

    private Map<String, StrategyFactory> strategyInstances = new HashMap<>();
    private Map<String, Indicator<AnalysisResult>> analyzerInstances = new HashMap<>();

    @Autowired
    BossaSingleFileDataProviderService dataProvider;

    @Autowired
    SignalRecordRepository signalRecordRepository;

    @Autowired
    AnalysisRecordRepository analysisRecordRepository;

    private StrategyFactory getStrategyFactory(String strategyFactoryClassName) {
        if (!strategyInstances.containsKey(strategyFactoryClassName)) {
            strategyInstances.put(strategyFactoryClassName, buildStrategyFactory(strategyFactoryClassName));
        }
        return strategyInstances.get(strategyFactoryClassName);
    }

    private StrategyFactory buildStrategyFactory(String strategyFactoryClassName) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(strategyFactoryClassName);
            Constructor<?> ctor = clazz.getConstructor();
            StrategyFactory result = (StrategyFactory) ctor.newInstance();
            return result;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private Indicator<AnalysisResult> getAnalyzer(String analyzerClassName, TimeSeries series) {
        return buildAnalyzer(analyzerClassName, series);
    }

    private Indicator<AnalysisResult> buildAnalyzer(String className, TimeSeries series) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(className);
            Constructor<?> ctor = clazz.getConstructor(TimeSeries.class);
            Indicator<AnalysisResult> result = (Indicator<AnalysisResult>) ctor.newInstance(series);
            return result;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private void checkSignals(ScanRecord scan, String stockName, TimeSeries series) {
        logger.trace("scanning : {}", stockName);
        if (series.getTickCount() > 0) {
            for (String strategyClassName : strategies) {
                StrategyFactory strategyFactory = getStrategyFactory(strategyClassName);
                Strategy strategy = strategyFactory.buildStrategy(series);
                TimeSeriesManager manager = new TimeSeriesManager(series);
                TradingRecord tradingRecord = manager.run(strategy);
                double buyAndHoldGain = new BuyAndHoldCriterion().calculate(series, tradingRecord);
                double strategyGain = new MBankProfitCriterion().calculate(series, tradingRecord);
                double avgTransTime = new AvgTransactionTimeCriterion().calculate(series, tradingRecord);

                List<SignalRecord> result = new ArrayList<>();
                for (Trade trade : tradingRecord.getTrades()) {
                    String signalRec = "";
                    String configuration = StrategyFactoryJsonizer.serialize(strategyFactory);
                    String note = "";
                    if ((trade.getEntry() != null)) {
                        ZonedDateTime date = series.getTick(trade.getEntry().getIndex()).getBeginTime();
                        signalRec = "";
                        if (trade.getEntry().isBuy()) signalRec = "BUY";
                        SignalRecord signal = new SignalRecord(scan, strategyFactory.getStrategyFactoryClassName(), strategyFactory.getName(), stockName, date, "", signalRec, configuration, strategyGain, buyAndHoldGain, note, avgTransTime);
                        signalRecordRepository.save(signal);
                    }
                    if ((trade.getExit() != null)) {
                        ZonedDateTime date = series.getTick(trade.getExit().getIndex()).getBeginTime();
                        signalRec = "";
                        if (trade.getExit().isSell()) signalRec = "SELL";
                        SignalRecord signal = new SignalRecord(scan, strategyFactory.getStrategyFactoryClassName(), strategyFactory.getName(), stockName, date, "", signalRec, configuration, strategyGain, buyAndHoldGain, note, avgTransTime);
                        signalRecordRepository.save(signal);
                    }
                }
                if ((tradingRecord.getLastOrder() != null) && (tradingRecord.getLastOrder().isBuy())) {
                    Order order = tradingRecord.getLastOrder();
                    String signalRec = order.isBuy() ? "BUY" : "SELL";
                    String configuration = StrategyFactoryJsonizer.serialize(strategyFactory);
                    String note = " ";
                    ZonedDateTime date = series.getTick(order.getIndex()).getBeginTime();
                    SignalRecord signal = new SignalRecord(scan, strategyFactory.getStrategyFactoryClassName(), strategyFactory.getName(), stockName, date, "", signalRec, configuration, strategyGain, buyAndHoldGain, note, avgTransTime);
                    signalRecordRepository.save(signal);
                }
            }
        } else {
            logger.warn("stock : {} tick series size = 0", stockName);
        }
    }

    private void makeAnalysis(ScanRecord scan, String stockName, TimeSeries series) {
        if (series.getTickCount() > 0) {
            for (String analyzerClassName : analyzers) {
                Indicator<AnalysisResult> analyzer = getAnalyzer(analyzerClassName, series);
                AnalysisRecord analysisRecord = new AnalysisRecord(scan, series.getLastTick().getBeginTime(), stockName, analyzer.getValue(series.getTickCount() - 1));
                analysisRecordRepository.save(analysisRecord);
            }
        } else {
            logger.warn("stock : {} time series size = 0", stockName);
        }
    }

    public void scanAll(ScanRecord scan) {
        for (String stock : stocks) {
            scan(scan, stock);
        }
    }

    public void scan(ScanRecord scan, String stockName) {
        TimeSeries series = dataProvider.load(stockName);
        checkSignals(scan, stockName, series);
        makeAnalysis(scan, stockName, series);
    }

    public List<String> getStocks() {
        return stocks;
    }

    public List<String> getStrategies() {
        return strategies;
    }

    public List<String> getAnalyzers() {
        return analyzers;
    }

    public void setStocks(List<String> stocks) {
        this.stocks = stocks;
    }

    public void setStrategies(List<String> strategies) {
        this.strategies = strategies;
    }

    public void setAnalyzers(List<String> analyzers) {
        this.analyzers = analyzers;
    }

}