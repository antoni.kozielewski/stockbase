package org.hou.stocktank.platform.stockBase.timer;

import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ShellComponent
@ConfigurationProperties(prefix = "timer")
public class StockBaseTimerComponent {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(StockBaseTimerComponent.class);
    public static final String ID = "TIMER";
    private final static String DATA_REGEX = "([^#]+)#(.*)"; // TARGET#data
    private final static String EVENT_DEFINITION_REGEX = "([0-9]{1,2}:[0-9]{2})_(" + DATA_REGEX + ")"; // 20:00_TARGET#data
    private final static Pattern DATA_PATTERN = Pattern.compile(DATA_REGEX);
    private final static Pattern EVENT_DEFINITION_PATTERN = Pattern.compile(EVENT_DEFINITION_REGEX);
    private boolean needStartup = true;
    private List<String> cronEvents;
    private List<String> startupEvents;
    private Map<String, List<String>> parsedEvents = new HashMap<>();

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @PostConstruct
    public void configure() {
        parseAndAddAllEvents();
    }

    public void executeStartupEvents() {
        if (startupEvents != null) {
            if (startupEvents.size() > 0) {
                for (String event : startupEvents) {
                    Matcher dataMatcher = DATA_PATTERN.matcher(event);
                    if (dataMatcher.find()) {
                        String target = dataMatcher.group(1);
                        String data = dataMatcher.group(2);
                        applicationEventPublisher.publishEvent(new StockBaseEvent(this, ID, target, StockBaseEvent.Type.EXECUTE, data));
                    } else {
                        logger.error("startup event regex not found in definition: {}", event);
                    }
                }
            } else {
                logger.debug("There is no events defined for startup.");
            }
        } else {
            logger.info("Timer has no configuration for startup events.");
        }
    }


    @Scheduled(fixedRate = 60000)
    public void execute() {
        if (needStartup) {
            executeStartupEvents();
            needStartup = false;
        }
        if (parsedEvents == null) {
            parseAndAddAllEvents();
        }
        String now = getNowTime();
        if (parsedEvents.containsKey(now)) {
            logger.trace("there are {} cronEvents defined for {}", parsedEvents.get(now).size(), now);
            for (String msg : parsedEvents.get(now)) {
                Matcher dataMatcher = DATA_PATTERN.matcher(msg);
                if (dataMatcher.find()) {
                    String target = dataMatcher.group(1);
                    String data = dataMatcher.group(2);
                    applicationEventPublisher.publishEvent(new StockBaseEvent(this, ID, target, StockBaseEvent.Type.EXECUTE, data));
                } else {
                    logger.error("data regex not found in msg: {}", msg);
                }
            }
        } else {
            logger.trace("no cronEvents defined for : {}", now);
        }
    }

    private void showConfiguration() {
        System.out.println("Timer configuration:");
        System.out.println("    startup events:");
        startupEvents.stream().forEach(event -> System.out.println("            '" + event + "'"));
        System.out.println("    cron events:");
        for (String time : parsedEvents.keySet()) {
            System.out.println("      " + time + ":");
            for (String msg : parsedEvents.get(time)) {
                System.out.println("            '" + msg + "'");
            }
        }

    }

    @ShellMethod("Timer management.")
    public String timer(@ShellOption(defaultValue = "") String command) {
        switch (command) {
            case "config":
                showConfiguration();
                return "";
            case "help":
            default:
                String res = "Timer management - using:\n";
                res += "    'timer config' - shows current configuration\n";
                return res;
        }
    }

    private String getNowTime() {
        DateTime now = DateTime.now();
        return now.getHourOfDay() + ":" + ((now.getMinuteOfHour() < 10) ? "0" + now.getMinuteOfHour() : now.getMinuteOfHour());
    }

    private void parseAndAddEvent(String event) {
        Matcher matcher = EVENT_DEFINITION_PATTERN.matcher(event);
        if (matcher.find()) {
            String time = matcher.group(1);
            String msg = matcher.group(2);
            if (!parsedEvents.containsKey(time)) {
                parsedEvents.put(time, new ArrayList<>());
            }
            parsedEvents.get(time).add(msg);
        } else {
            logger.error("Timer event definition '{}'- does not match : {} - THIS TIMER WILL NOT BE PROCESSED !!!!", event, EVENT_DEFINITION_REGEX);
        }
    }

    private void parseAndAddAllEvents() {
        Pattern pattern = Pattern.compile(EVENT_DEFINITION_REGEX);
        if (cronEvents != null) {
            for (String event : cronEvents) {
                parseAndAddEvent(event);
            }
        } else {
            logger.warn("Timer has no configuration for planned events!");
            parsedEvents = new HashMap<>();
        }
    }

    public List<String> getCronEvents() {
        return cronEvents;
    }

    public void setCronEvents(List<String> cronEvents) {
        this.cronEvents = cronEvents;
    }

    public List<String> getStartupEvents() {
        return startupEvents;
    }

    public void setStartupEvents(List<String> startupEvents) {
        this.startupEvents = startupEvents;
    }
}
