package org.hou.stocktank.platform.stockBase.stocktank.storage;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
public class ScanRecord {

    @Id
    private String id;

    @Column(nullable = false)
    private ZonedDateTime date;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ScanType type;


    public ScanRecord() {
        super();
    }

    public ScanRecord(ScanType type, ZonedDateTime date) {
        this(type.toString() + "_" + date.toLocalDate().toString().replace(":","_"), type, date);
    }

    public ScanRecord(String id, ScanType type, ZonedDateTime date) {
        this.id = id;
        this.type = type;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public ScanType getType() {
        return type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public void setType(ScanType type) {
        this.type = type;
    }
}
