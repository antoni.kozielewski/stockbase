package org.hou.stocktank.platform.stockBase.stocktank.storage;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SignalRecordRepository extends CrudRepository<SignalRecord, Long> {
    List<SignalRecord> findByScanIdAndStock(String scanId, String stock);

    @Query("select s from SignalRecord s where s.scanId = :scanId and s.stock = :stock and s.id in (select max(x.id) from SignalRecord x group by x.stock, x.strategyName)")
    List<SignalRecord> findLastSignalsByScanIdAndStock(@Param("scanId") String scanId, @Param("stock") String stock);

    @Query("select s from SignalRecord s where " +
            "s.scanId = :scanId and " +
            "s.stock = :stock and " +
            "s.strategyClassName = :strategyClassName " +
            "order by s.timestampLong desc")
    List<SignalRecord> findLastStrategySignalsByScanIdAndStock(@Param("scanId") String scanId, @Param("strategyClassName") String strategyClassName, @Param("stock") String stock);

    @Query("select s.stock from SignalRecord s where " +
            "s.scanId = :scanId and " +
            "s.strategyClassName = :strategyClassName and " +
            "s.timestampLong >= :fromMillis " +
            "group by s.stock, s.timestampLong " +
            "order by s.timestampLong desc")
    List<String> findStocksByScanIdAndStrategySignalAfterTimestamp(@Param("scanId") String scanId, @Param("strategyClassName") String strategyClassName, @Param("fromMillis") Long fromMillis);


    @Query("select s.stock from SignalRecord s where " +
            "s.scanId = :scanId and " +
            "s.strategyClassName = :strategyClassName and " +
            "s.timestampLong >= :fromMillis and " +
            "s.signal = :signalType " +
            "group by s.stock " +
            "order by s.timestampLong desc")
    List<String> findStocksByScanIdAndStrategySignalAfterTimestamp(@Param("scanId") String scanId, @Param("strategyClassName") String strategyClassName, @Param("signalType") String signalType, @Param("fromMillis") Long fromMillis);

    @Query("select s.stock from SignalRecord s where " +
            "s.scanId = :scanId and " +
            "s.id in (select max(x.id) from SignalRecord x group by x.stock, x.strategyName) " +
            "and s.timestampLong >= :fromMillis " +
            "and s.signal = :signalType " +
            "group by s.stock " +
            "having count(s.strategyName) >= :n " +
            "order by count(s.strategyName) desc"
    )
    List<String> findStocksByScanIdAndNSignalsAfterTimestamp(@Param("scanId") String scanId, @Param("n") Long n, @Param("signalType") String signalType, @Param("fromMillis") Long fromMillis);
}
