package org.hou.stocktank.platform.stockBase.utils;

import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;
import org.hou.stocktank.platform.stockBase.stocktank.StockTankEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Component
@ConfigurationProperties(prefix = "raport-file-storage")
public class StockBaseRaportFileStorageComponent {
    private final static Logger logger = LoggerFactory.getLogger(StockBaseRaportFileStorageComponent.class);
    private String storagePath;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;


    @Async
    @EventListener(classes = StockTankEvent.class)
    public void saveStockTankRaport(Object eventSrc) throws IOException {
        StockTankEvent event = (StockTankEvent) eventSrc;
        if (event.getType().equals(StockBaseEvent.Type.NOTIFICATION) && event.getValue().equals(StockTankEvent.VALUE_REPORT_READY)) {
            logger.trace("event received - raport will be saved to file ");
            String filename = getSafeStoragePath() + event.getData().get(StockTankEvent.DATA_KEY_SCAN_ID) + ".txt";
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
            writer.write(event.getData().get(StockTankEvent.DATA_KEY_REPORT));
            writer.close();
            logger.debug("event received - raport (scanId = {}) will be saved to file {}", event.getData().get(StockTankEvent.DATA_KEY_SCAN_ID), filename);
            publishEvent(event.getData().get(StockTankEvent.DATA_KEY_SCAN_ID), filename);
        }
    }

    private void publishEvent(String scanId, String filePath) {
        StockTankEvent event = new StockTankEvent(this, StockBaseEvent.ALL, StockBaseEvent.Type.NOTIFICATION, StockTankEvent.VALUE_REPORT_FILE_READY);
        event.getData().put(StockTankEvent.DATA_KEY_SCAN_ID, scanId);
        event.getData().put(StockTankEvent.DATA_KEY_FILE_PATH, filePath);
        applicationEventPublisher.publishEvent(event);
    }

    private String getSafeStoragePath() {
        return storagePath.endsWith(File.separator) ? storagePath : storagePath + File.separator;
    }


    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }
}
