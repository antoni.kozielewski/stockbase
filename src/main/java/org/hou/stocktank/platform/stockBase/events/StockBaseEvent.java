package org.hou.stocktank.platform.stockBase.events;

import org.springframework.context.ApplicationEvent;

import java.util.HashMap;
import java.util.Map;


public class StockBaseEvent extends ApplicationEvent {
    public final static String ALL = "ALL";
    public final static String VALUE = "value";
    private String sender;
    private String target;
    private Type type;
    private Map<String, String> data = new HashMap<>();

    public enum Type {
        EXECUTE,
        NOTIFICATION;
    }

    public StockBaseEvent(Object source, String sender, String target, Type type) {
        super(source);
        this.sender = sender;
        this.type = type;
        this.target = target;
    }

    public StockBaseEvent(Object source, String sender, String target, Type type, String value) {
        this(source, sender, target, type);
        this.data.put(VALUE, value);
    }

    public StockBaseEvent(Object source, String sender, String target, Type type, Map<String, String> data) {
        super(source);
        this.sender = sender;
        this.target = target;
        this.type = type;
        this.data = data;
    }

    public void addData(String key, String value) {
        this.data.put(key, value);
    }

    public Type getType() {
        return type;
    }

    public String getValue() {
        return data.get(VALUE);
    }

    public String getSender() {
        return sender;
    }

    public String getTarget() {
        return target;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void putData(String key, String value) {
        data.put(key, value);
    }

    public String toString() {
        return type + " | sender : " + sender + " | target: " + target + " | " + data;
    }
}
