package org.hou.stocktank.platform.stockBase.stocktank.reports.txt;


import org.hou.stocktank.analysis.MarketState;
import org.hou.stocktank.platform.stockBase.stocktank.storage.*;
import org.hou.stocktank.strategies.IchimokuStrategyFactory;
import org.hou.stocktank.strategies.ParabolicStopAndReverseStrategyFactory;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@ConfigurationProperties(prefix = "stocktank")
public class StockTankTXTReportBuilderService {
    private final static Logger logger = LoggerFactory.getLogger(StockTankTXTReportBuilderService.class);
    private List<String> walletStocks = new ArrayList<>();
    private int signalsTimeWindowInDays;
    private int maxScanningDays;
    private Long minSignalsForBestBuys;
    private ReportBuilder reportBuilder;
    private double minimumAvgVolume;

    @Autowired
    ScanRecordRepository scanRepository;

    @Autowired
    SignalRecordRepository signalRepository;

    @Autowired
    AnalysisRecordRepository analysisRepository;

    @Autowired
    StockInfoService stockInfoService;


    private class ReportBuilder {
        private StringBuilder reportBuilder = new StringBuilder();

        private void appendSeparator() {
            reportBuilder.append("\n");
            reportBuilder.append("\n");
            reportBuilder.append("\n");
            reportBuilder.append("\n");
        }

        public void appendHeader(String title) {
            appendSeparator();
            reportBuilder.append(ReportTXTUtil.fillCenter("", "#", 167)).append("\n");
            reportBuilder.append(ReportTXTUtil.fillCenter("", "#", 167)).append("\n");
            reportBuilder.append(ReportTXTUtil.fillCenter("", " ", 167)).append("\n");
            reportBuilder.append(ReportTXTUtil.fillCenter(ReportTXTUtil.fillCenter(title, " ", 80), "#", 167)).append("\n");
            reportBuilder.append(ReportTXTUtil.fillCenter("", " ", 167)).append("\n");
            reportBuilder.append(ReportTXTUtil.fillCenter("", "#", 167)).append("\n");
            reportBuilder.append(ReportTXTUtil.fillCenter("", "#", 167)).append("\n");
        }

        private void appendStockInfo(String stockName, Map<String, String> stockInfoData) {
            reportBuilder.append(ReportTXTUtil.fillCenter("", "-", 167)).append("\n");
            reportBuilder.append(ReportTXTUtil.fillRight("|  " + stockName, " ", 166) + "|\n").append("\n");
            for (String key : stockInfoData.keySet()) {
                reportBuilder.append("|" + ReportTXTUtil.fillLeft(key, " ", 63) + ": " + ReportTXTUtil.fillRight(stockInfoData.get(key), " ", 100) + "|\n");
            }
            reportBuilder.append(ReportTXTUtil.fillCenter("", "-", 167)).append("\n");
        }

        private void appendSignals(List<SignalRecord> signals, int maxScanningDays) {
            reportBuilder.append("\n");
            List<SignalRecord> buys = signals.stream().filter(x -> x.getSignal().equals("BUY")).collect(Collectors.toList());
            List<SignalRecord> sells = signals.stream().filter(x -> x.getSignal().equals("SELL")).collect(Collectors.toList());
            Collections.sort(buys, Comparator.comparing(SignalRecord::getDate).reversed());
            Collections.sort(sells, Comparator.comparing(SignalRecord::getDate));
            buys.stream().forEach(b -> {
                b.setStrategyClassName(""); // remove StrategyClassName
                reportBuilder.append(ReportTXTUtil.prettyString(b, maxScanningDays)).append("\n");
            });
            reportBuilder.append("\n");
            reportBuilder.append("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  |          |          |          |          |          |\n");
            reportBuilder.append("\n");
            sells.stream().forEach(s -> {
                s.setStrategyClassName(""); // remove StrategyClassName
                reportBuilder.append(ReportTXTUtil.prettyString(s, maxScanningDays)).append("\n");
            });
            reportBuilder.append("\n");
        }

        private void appendAnalysis(List<AnalysisRecord> analysisRecords) {
            reportBuilder.append("\n");
            analysisRecords.stream()
                    .filter(x -> x != null)
                    .filter(x -> !x.getMarketState().equals(MarketState.UNKNOWN.name()))
                    .forEach(x -> reportBuilder.append(ReportTXTUtil.prettyString(x)).append("\n"));
            reportBuilder.append("\n");
        }

        public void appendStockReport(String stockName, List<SignalRecord> signals, List<AnalysisRecord> analysisRecords, Map<String, String> stockInfoData, int maxScanningDays) {
            appendSeparator();
            appendStockInfo(stockName, stockInfoData);
            appendSignals(signals, maxScanningDays);
            appendAnalysis(analysisRecords);
        }

        public String toString() {
            return reportBuilder.toString();
        }
    }

    public String generateReport(String scanId) {
        reportBuilder = new ReportBuilder();
        appendSAR(scanId);
        appendIchimoku(scanId);
        appendWithNumberOfSignals(scanId);

        reportBuilder.appendHeader(" WALLET STOCKS ");
        addAllStocksToReport(scanId, walletStocks);
        return reportBuilder.toString();
    }

    private Long getFromisEpoch(int days) {
        return DateTime.now().minusDays(days).getMillis() / 1000;
    }

    private void addAllStocksToReport(String scanId, List<String> stocks) {
        StringBuilder stocksBuilder = new StringBuilder();
        for (String stockName : stocks) {
            List<SignalRecord> signals = signalRepository.findLastSignalsByScanIdAndStock(scanId, stockName);
            List<AnalysisRecord> analysis = analysisRepository.findByScanIdAndStock(scanId, stockName);
            Map<String, String> info = stockInfoService.getInfoMap(stockName);
            reportBuilder.appendStockReport(stockName, signals, analysis, info, maxScanningDays);
        }
    }

    private int getDaysBack() {
        int daysBack = 1;
        if (DateTime.now().getDayOfWeek() == 6) {
            daysBack = 2;
        } // saturday
        if (DateTime.now().getDayOfWeek() == 7) {
            daysBack = 3;
        } // sunday
        if (DateTime.now().getDayOfWeek() == 1) {
            daysBack = 4;
        } // monday morning
        return daysBack;
    }

    private void appendSAR(String scanId) {
        List<String> sarBuys = signalRepository.findStocksByScanIdAndStrategySignalAfterTimestamp(scanId, ParabolicStopAndReverseStrategyFactory.class.getName(), "BUY", getFromisEpoch(getDaysBack()));
        reportBuilder.appendHeader("SAR RECOMMENDATIONS [last " + getDaysBack() + " days]");
        addAllStocksToReport(scanId, sarBuys);
    }

    private void appendIchimoku(String scanId) {
        List<String> ichimokuBuys = signalRepository.findStocksByScanIdAndStrategySignalAfterTimestamp(scanId, IchimokuStrategyFactory.class.getName(), getFromisEpoch(getDaysBack()));
        reportBuilder.appendHeader("ICHIMOKU RECOMMENDATIONS [last " + getDaysBack() + " days]");
        addAllStocksToReport(scanId, ichimokuBuys);
    }

    private void appendWithNumberOfSignals(String scanId) {
        List<String> bestBuys = signalRepository.findStocksByScanIdAndNSignalsAfterTimestamp(scanId, minSignalsForBestBuys, "BUY", getFromisEpoch(signalsTimeWindowInDays + getDaysBack()));
        reportBuilder.appendHeader("BUY RECOMMENDATIONS ");
        List<String> filtered = bestBuys.stream().filter(stockName -> stockInfoService.getNSessionsAvgVolumen(stockName) >= minimumAvgVolume).collect(Collectors.toList());
        addAllStocksToReport(scanId, filtered);
    }

    public List<String> getWalletStocks() {
        return walletStocks;
    }

    public void setWalletStocks(List<String> walletStocks) {
        this.walletStocks = walletStocks;
    }

    public int getMaxScanningDays() {
        return maxScanningDays;
    }

    public void setMaxScanningDays(int maxScanningDays) {
        this.maxScanningDays = maxScanningDays;
    }

    public Long getMinSignalsForBestBuys() {
        return minSignalsForBestBuys;
    }

    public void setMinSignalsForBestBuys(Long minSignalsForBestBuys) {
        this.minSignalsForBestBuys = minSignalsForBestBuys;
    }

    public int getSignalsTimeWindowInDays() {
        return signalsTimeWindowInDays;
    }

    public void setSignalsTimeWindowInDays(int signalsTimeWindowInDays) {
        this.signalsTimeWindowInDays = signalsTimeWindowInDays;
    }

    public double getMinimumAvgVolume() {
        return minimumAvgVolume;
    }

    public void setMinimumAvgVolume(double minimumAvgVolume) {
        this.minimumAvgVolume = minimumAvgVolume;
    }
}
