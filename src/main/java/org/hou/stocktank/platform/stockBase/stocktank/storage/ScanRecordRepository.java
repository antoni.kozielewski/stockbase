package org.hou.stocktank.platform.stockBase.stocktank.storage;


import org.joda.time.DateTime;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScanRecordRepository extends CrudRepository<ScanRecord, String> {

    List<ScanRecord> findByDate(DateTime date);

}
