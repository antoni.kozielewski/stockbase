package org.hou.stocktank.platform.stockBase.stocktank;

import org.hou.stocktank.platform.stockBase.downloader.FileDownloaderEvent;
import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;
import org.hou.stocktank.platform.stockBase.stocktank.storage.ScanRecord;
import org.hou.stocktank.platform.stockBase.stocktank.storage.ScanRecordRepository;
import org.hou.stocktank.platform.stockBase.stocktank.storage.ScanType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.shell.standard.ShellComponent;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

@ShellComponent
@ConfigurationProperties(prefix = "stocktank")
public class StockTankComponent {
    private final static Logger logger = LoggerFactory.getLogger(StockTankComponent.class);
    public final static String ID = "STOCKTANK";
    private final static String BOSSA_FILE_KEY = "bossa";
    private String filesPath;

    @Autowired
    ScanRecordRepository scanRecordRepository;

    @Autowired
    StockTankScannerService scannerService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;


    // FIXME REMOVE THAT SHIT - only for manual scan execution
    @Async
    @EventListener(classes = StockBaseEvent.class)
    public void tempEventListener(Object eventSrc) {
        StockBaseEvent event = (StockBaseEvent) eventSrc;
        if (event.getTarget().equals(ID)) {
            scan();
        }
    }

    @Async
    @EventListener(classes = FileDownloaderEvent.class)
    public void trigger(Object eventSrc) {
        FileDownloaderEvent event = (FileDownloaderEvent) eventSrc;
        if (event.getType().equals(StockBaseEvent.Type.NOTIFICATION) && event.getValue().equals(FileDownloaderEvent.DATA_VALUE_DOWNLOAD_FINISHED)) {
            if (event.getData().get(FileDownloaderEvent.DATA_FILEKEY).equals(BOSSA_FILE_KEY)) {
                logger.debug("event received - new bossa file is ready for scan");
                scan();
            } else {
                logger.info("received event - but file key {} != {}", event.getData().get(FileDownloaderEvent.DATA_FILEKEY), BOSSA_FILE_KEY);
            }
        }
    }


    private void scan() {
        ScanRecord scan = scanRecordRepository.save(new ScanRecord(ScanType.STOCKTANK, ZonedDateTime.now()));
        logger.info("stocktank scan start.");
        scannerService.scanAll(scan);
        logger.info("stocktank scan finished.");
        Map<String, String> data = new HashMap<>();
        data.put(StockBaseEvent.VALUE, StockTankEvent.VALUE_SCAN_READY);
        data.put(StockTankEvent.DATA_KEY_SCAN_ID, scan.getId());
        applicationEventPublisher.publishEvent(new StockTankEvent(this, StockBaseEvent.ALL, StockBaseEvent.Type.NOTIFICATION, data));
        logger.info("stocktank scan event published.");
    }


    public String getFilesPath() {
        return filesPath;
    }

    public void setFilesPath(String filesPath) {
        this.filesPath = filesPath;
    }
}
