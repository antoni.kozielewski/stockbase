package org.hou.stocktank.platform.stockBase.stocktank.storage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnalysisRecordRepository extends CrudRepository<AnalysisRecord, String> {

    List<AnalysisRecord> findByScanIdAndStock(String scanId, String stock);
}
