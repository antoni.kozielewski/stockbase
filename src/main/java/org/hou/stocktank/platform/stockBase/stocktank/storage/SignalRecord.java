package org.hou.stocktank.platform.stockBase.stocktank.storage;


import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Date;

@Entity
public class SignalRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, updatable = false, insertable = false)
    private String scanId;

    @ManyToOne
    @JoinColumn(name = "scanId")
    private ScanRecord scan;

    @Column(nullable = false)
    private ZonedDateTime date;

    @Column(nullable = false)
    private Long timestampLong;

    @Column(nullable = false)
    private String stock;

    @Column(nullable = false)
    private String signal;

    @Column(nullable = false)
    private String hash;

    @Column
    private String strategyName;

    @Column
    private String strategyClassName;

    @Column(nullable = false, length = 9999)
    private String configuration;

    @Column(nullable = false)
    private double strategyGain;

    @Column(nullable = false)
    private double avgTransTime;

    @Column(nullable = false)
    private double buyAndHoldGain;

    @Column
    private String note;


    public SignalRecord() {

    }

    public SignalRecord(ScanRecord scan, String strategyFactoryClassName, String strategyName, String stock, ZonedDateTime date, String hash, String signal, String configuration, double strategyGain, double buyAndHoldGain, String note, Double avgTransTime) {
        this();
        this.scan = scan;
        this.scanId = scan.getId();
        this.strategyClassName = strategyFactoryClassName;
        this.strategyName = strategyName;
        this.stock = stock;
        this.date = date;
        this.timestampLong = date.toEpochSecond();
        this.hash = hash;
        this.signal = signal;
        this.strategyGain = strategyGain;
        this.buyAndHoldGain = buyAndHoldGain;
        this.configuration = configuration;
        this.note = note;
        this.avgTransTime = avgTransTime;
    }

    public Date getDate() {
        return Date.from(date.toInstant());
    }

    public ZonedDateTime getDateTime() {
        return date;
    }

    public String getHash() {
        return hash;
    }

    public String getSignal() {
        return signal;
    }

    public String getConfiguration() {
        return configuration;
    }

    public String getNote() {
        return note;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public void setStrategyGain(double strategyGain) {
        this.strategyGain = strategyGain;
    }

    public void setBuyAndHoldGain(double buyAndHoldGain) {
        this.buyAndHoldGain = buyAndHoldGain;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public Long getId() {
        return id;
    }

    public double getAvgTransTime() {
        return avgTransTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public void setAvgTransTime(double avgTransTime) {
        this.avgTransTime = avgTransTime;
    }

    public String getStock() {
        return stock;
    }

    public double getStrategyGain() {
        return strategyGain;
    }

    public double getBuyAndHoldGain() {
        return buyAndHoldGain;
    }

    public Long getTimestampLong() {
        return timestampLong;
    }

    public void setTimestampLong(Long timestampLong) {
        this.timestampLong = timestampLong;
    }

    public String getStrategyClassName() {
        return strategyClassName;
    }

    public void setStrategyClassName(String strategyClassName) {
        this.strategyClassName = strategyClassName;
    }

    public String getScanId() {
        return scanId;
    }

    public ScanRecord getScan() {
        return scan;
    }

    public void setScan(ScanRecord scan) {
        this.scan = scan;
    }

    @Override
    public String toString() {
        return id + " : " + hash + " : " + date + " - " + signal;
    }

}
