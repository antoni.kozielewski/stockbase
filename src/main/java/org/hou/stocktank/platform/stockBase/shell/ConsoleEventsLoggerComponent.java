package org.hou.stocktank.platform.stockBase.shell;

import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
@ConfigurationProperties(prefix = "show-events")
public class ConsoleEventsLoggerComponent {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ConsoleEventsLoggerComponent.class);
    private boolean showEvents;
    public final static String ID = "CONSOLE_EVENT_LOGGER";

    @Async
    @EventListener()
    public void execute(Object eventSrc) {
        logger.trace("Event: '{}'", eventSrc);
        if (showEvents && eventSrc instanceof StockBaseEvent) {
            System.out.println("EVENT LOGGER : " + eventSrc);
        }
    }

    @ShellMethod("Console events logger management.")
    public String showEvents(@ShellOption(defaultValue = "") String option) {
        switch (option) {
            case "on":
                showEvents = true;
                return "Console events logging has been turned on.";
            case "off":
                showEvents = false;
                return "Console events logging has been turned off.";
            case "":
                return "Console events logger " + (showEvents ? "on" : "off");
            case "help":
            default:
                String res = "Console events logger management - using: \n";
                res += "    'show-events on'  - turns on logging\n";
                res += "    'show-events off' - turns off logging\n";
                res += "    'show-events'     - current status\n";
                return res;
        }

    }

    public boolean isShowEvents() {
        return showEvents;
    }

    public void setShowEvents(boolean showEvents) {
        this.showEvents = showEvents;
    }
}
