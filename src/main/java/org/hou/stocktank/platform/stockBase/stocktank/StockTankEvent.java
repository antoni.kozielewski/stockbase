package org.hou.stocktank.platform.stockBase.stocktank;

import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;

import java.util.Map;

public class StockTankEvent extends StockBaseEvent {
    public static final String DATA_KEY_SCAN_ID = "scanId";
    public static final String DATA_KEY_REPORT = "report";
    public static final String DATA_KEY_FILE_PATH = "filePath";

    public static final String VALUE_SCAN_READY = "scan ready";
    public static final String VALUE_REPORT_READY = "report ready";
    public static final String VALUE_REPORT_FILE_READY = "report file ready";

    public StockTankEvent(Object source, String target, Type type) {
        super(source, StockTankComponent.ID, target, type);
    }

    public StockTankEvent(Object source, String target, Type type, String value) {
        super(source, StockTankComponent.ID, target, type, value);
    }

    public StockTankEvent(Object source, String target, Type type, Map<String, String> data) {
        super(source, StockTankComponent.ID, target, type, data);
    }
}
