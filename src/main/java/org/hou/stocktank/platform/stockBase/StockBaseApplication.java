package org.hou.stocktank.platform.stockBase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

@Configuration
@EnableAsync
@EnableConfigurationProperties
@EnableScheduling
@SpringBootApplication
public class StockBaseApplication {

	public static void main(String[] args) {

		DecimalFormat df = new DecimalFormat("#####0.00");
		DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
		dfs.setGroupingSeparator('_');
		dfs.setDecimalSeparator('.');
		df.setDecimalFormatSymbols(dfs);

		SpringApplication.run(StockBaseApplication.class, args);
	}
}
