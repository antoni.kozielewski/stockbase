package org.hou.stocktank.platform.stockBase.stocktank.reports.txt;

import org.hou.stocktank.platform.stockBase.stocktank.BossaSingleFileDataProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ta4j.core.TimeSeries;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class StockInfoService {
    public final static int NUMBER_OF_SESSIONS = 5;
    public final static String CHART_URL = "chart";
    public final static String CLOSE_PRICE = "close price  ";
    public final static String FIVE_SESSIONS_AVG_VOLUMEN = "avg vol (" + String.valueOf(NUMBER_OF_SESSIONS) + " sessions)  ";
    public final static String FIVE_SESSIONS_CHANGE = "change  (5 sessions)  ";
    public final static String TEN_SESSIONS_CHANGE = "change  (10 sessions) ";
    public final static String TWENTY_SESSIONS_CHANGE = "change  (20 sessions) ";


    @Autowired
    BossaSingleFileDataProviderService dataProvider;

    public Map<String, String> getInfoMap(String stockName) {
        Map<String, String> result = new LinkedHashMap<>();
        result.put(CHART_URL, getChartUrl(stockName));
        TimeSeries series = dataProvider.load(stockName);
        result.put(CLOSE_PRICE, String.valueOf(series.getTick(series.getEndIndex()).getClosePrice().toDouble()));
        result.put(FIVE_SESSIONS_AVG_VOLUMEN, getNSessionsAvgVolumenString(series));
        result.put(FIVE_SESSIONS_CHANGE, getNSessionsChange(5, series));
        result.put(TEN_SESSIONS_CHANGE, getNSessionsChange(10, series));
        result.put(TWENTY_SESSIONS_CHANGE, getNSessionsChange(20, series));
        return result;
    }

    private String getChartUrl(String stockName) {
        return "http://www.bankier.pl/inwestowanie/profile/quote.html?symbol=" + stockName.toUpperCase();
    }

    public double getNSessionsAvgVolumen(String stockName) {
        Map<String, String> result = new LinkedHashMap<>();
        result.put(CHART_URL, getChartUrl(stockName));
        TimeSeries series = dataProvider.load(stockName);
        return getNSessionsAvgVolumen(series);
    }

    private double getNSessionsAvgVolumen(TimeSeries series) {
        double value = 0d;
        double volumen = 0;
        for (int i = series.getTickCount() - NUMBER_OF_SESSIONS - 1; i < series.getTickCount(); i++) {
            volumen = volumen + series.getTick(i).getVolume().toDouble();
            value = value + series.getTick(i).getClosePrice().toDouble();
        }
        return (Math.round(100 * volumen * value / NUMBER_OF_SESSIONS)) / 100;
    }

    private String getNSessionsAvgVolumenString(TimeSeries series) {
        return getNSessionsAvgVolumen(series) +  " PLN";
    }

    private String getNSessionsChange(int n, TimeSeries series) {
        double start = series.getTick(series.getTickCount() - n - 1).getClosePrice().toDouble();
        double end = series.getLastTick().getClosePrice().toDouble();
        return String.valueOf(String.format("%.2f", (Math.round(10000 * (end - start) / start)) / 100d)) + " %";
    }


}
