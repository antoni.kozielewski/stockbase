package org.hou.stocktank.platform.stockBase.stocktank;

import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;
import org.hou.stocktank.platform.stockBase.stocktank.reports.txt.StockTankTXTReportBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.shell.standard.ShellComponent;

@ShellComponent
@ConfigurationProperties("stocktank")
public class StockTankRaportComponent {
    private final static Logger logger = LoggerFactory.getLogger(StockTankRaportComponent.class);

    @Autowired
    private StockTankTXTReportBuilderService stockTankRaportBuilderService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Async
    @EventListener(classes = StockTankEvent.class)
    public void execute(Object eventSrc) {
        StockTankEvent event = (StockTankEvent) eventSrc;
        if (event.getType().equals(StockBaseEvent.Type.NOTIFICATION) && event.getValue().equals(StockTankEvent.VALUE_SCAN_READY)) {
            String scanId = event.getData().get(StockTankEvent.DATA_KEY_SCAN_ID);
            logger.debug("event received - scan [{}]finished - raport should be generated.", scanId);
            if (scanId == null || scanId.isEmpty()) {
                logger.error("StockBaseEvent : ScanFinished event doesn't contain scanId data !!!!");
                return;
            }
            String raport = stockTankRaportBuilderService.generateReport(scanId);
            logger.debug("raport ready for scanId : {}", scanId);
            StockTankEvent raportReadyEvent = new StockTankEvent(this, StockBaseEvent.ALL, StockBaseEvent.Type.NOTIFICATION, StockTankEvent.VALUE_REPORT_READY);
            raportReadyEvent.addData(StockTankEvent.DATA_KEY_SCAN_ID, scanId);
            raportReadyEvent.addData(StockTankEvent.DATA_KEY_REPORT, raport);
            applicationEventPublisher.publishEvent(raportReadyEvent);
        }
    }
    
}
