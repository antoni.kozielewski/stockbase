package org.hou.stocktank.platform.stockBase.stocktank.storage;

import org.hou.stocktank.analysis.AnalysisResult;
import org.hou.stocktank.analysis.MarketState;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
public class AnalysisRecord {

    @Id
    private String id;

    @Column(nullable = false, updatable = false, insertable = false)
    private String scanId;

    @ManyToOne
    @JoinColumn(name = "scanId")
    private ScanRecord scan;

    @Column(nullable = false)
    private ZonedDateTime date;

    @Column(nullable = false)
    private String stock;

    @Column(nullable = false)
    private String marketState;

    @Column(nullable = false, length = 3000)
    private String description;

    @Column(nullable = false)
    private double value;

    @Column(nullable = false)
    private String source;

    @Column(nullable = false)
    private String sourceId;

    public AnalysisRecord() {
        super();
    }

    public AnalysisRecord(ScanRecord scan, ZonedDateTime date, String stock, AnalysisResult analysisResult) {
        this(date + stock + analysisResult.getSourceId(), scan, date, stock, analysisResult.getMarketState(), analysisResult.getDescription(), analysisResult.getValue(), analysisResult.getSource(), analysisResult.getSourceId());
    }

    public AnalysisRecord(String id, ScanRecord scan, ZonedDateTime date, String stock, MarketState marketState, String description, double value, String source, String sourceId) {
        this.id = id;
        this.scan = scan;
        this.date = date;
        this.stock = stock;
        this.marketState = marketState.name();
        this.description = description;
        this.value = value;
        this.source = source;
        this.sourceId = sourceId;
    }

    public String getId() {
        return id;
    }

    public String getScanId() {
        return scanId;
    }

    public ScanRecord getScan() {
        return scan;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public String getMarketState() {
        return marketState;
    }

    public String getDescription() {
        return description;
    }

    public double getValue() {
        return value;
    }

    public String getSource() {
        return source;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMarketState(String marketState) {
        this.marketState = marketState;
    }

    public void setMarketState(MarketState marketState) {
        this.marketState = marketState.name();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public DateTime getDateTime() {
        return new DateTime(date);
    }

    public String getStock() {
        return stock;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }
}
