package org.hou.stocktank.platform.stockBase.mailer;

import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;
import org.hou.stocktank.platform.stockBase.stocktank.StockTankEvent;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@ShellComponent
@ConfigurationProperties(prefix = "mailer")
public class StockBaseRaportMailerComponent {
    private final static Logger logger = LoggerFactory.getLogger(StockBaseRaportMailerComponent.class);
    private String server;
    private String from;
    private String password = "";
    private Map<String, String> properties;
    private List<String> recipients;

    @PostConstruct
    public void init(){
        System.out.println("----------------------------------------------------------------------------------------------");
        System.out.println("|                                                                                            |");
        System.out.println("|                                                                                            |");
        System.out.println("|                         MAILER NEED TO BE CONFIGURED !!!                                   |");
        System.out.println("|                                                                                            |");
        System.out.println("|                 example: mailer password ....p.a.s.s.w.o.r.d....                           |");
        System.out.println("|                                                                                            |");
        System.out.println("|                                                                                            |");
        System.out.println("----------------------------------------------------------------------------------------------");
    }

    @ShellMethod("set password")
    public String mailer(@ShellOption String command, @ShellOption String password) {
        if (command.equals("password")) {
            this.password = password;
            return "Password has been set.";
        }
        return "using: mailer password ____your_email_password___";
    }

    @Async
    @EventListener(StockTankEvent.class)
    public void sendStockTankRaport(StockTankEvent event) {
        if (password.isEmpty()) {
            logger.warn("Mailer is not fully configured! Please set password for mailer - example: mailer password __your_password__.");
            return;
        }
        if (event.getType().equals(StockBaseEvent.Type.NOTIFICATION) && event.getValue().equals(StockTankEvent.VALUE_REPORT_FILE_READY)) {
            logger.debug("event received - stockTank raport for scan: " + event.getData().get(StockTankEvent.DATA_KEY_SCAN_ID));
            sendEmail(prepareSubject(), event.getData().get(StockTankEvent.DATA_KEY_FILE_PATH), event.getData().get(StockTankEvent.DATA_KEY_SCAN_ID));
        }
    }

    private String prepareSubject() {
        return "[StockTank] - report :: " + DateTime.now().toString("YYYY-MM-dd");
    }

    private String prepareFileName() {
        return "report_" + DateTime.now().toString("YYYY-MM-dd") + ".txt";
    }

    private void sendEmail(String subject, String filePath, String scanId) {
        Properties props = new Properties();
        for (String key : properties.keySet()) {
            props.put(key, properties.get(key));
        }

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(from, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(String.join(",", recipients)));
            message.setSubject(subject);
            message.setText("StockTank Report : scanId = '" + scanId + "'");

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText("StockTank Report : scanId = '" + scanId + "'");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(filePath);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(prepareFileName());
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);


            Transport.send(message);
            logger.debug("email [{}]has been sent.", subject);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public String getServer() {
        return server;
    }

    public String getFrom() {
        return from;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
}
