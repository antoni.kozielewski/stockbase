package org.hou.stocktank.platform.stockBase.downloader;

import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;

import java.util.Map;

public class FileDownloaderEvent extends StockBaseEvent {
    public static final String DATA_VALUE_DOWNLOAD_FINISHED = "download finished";
    public static final String DATA_FILEPATH = "filePath";
    public static final String DATA_FILEKEY = "fileKey";

    public FileDownloaderEvent(Object source, String target, Type type) {
        super(source, StockBaseFileDownloaderComponent.ID, target, type);
    }

    public FileDownloaderEvent(Object source, String target, Type type, String value) {
        super(source, StockBaseFileDownloaderComponent.ID, target, type, value);
    }

    public FileDownloaderEvent(Object source, String target, Type type, Map<String, String> data) {
        super(source, StockBaseFileDownloaderComponent.ID, target, type, data);
    }
}
