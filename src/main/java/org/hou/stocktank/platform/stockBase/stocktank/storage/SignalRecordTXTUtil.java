package org.hou.stocktank.platform.stockBase.stocktank.storage;

import org.hou.stocktank.utils.StocktankTXTUtil;

import java.text.DecimalFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class SignalRecordTXTUtil extends StocktankTXTUtil {
    static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE;

    public static String generateBar(SignalRecord signalRecord, int max, int length) {
        return getBarString(max - (Duration.between(signalRecord.getDateTime(), ZonedDateTime.now()).getSeconds()/(3600*24)), max, length, "-") + ">";
    }

    public static String prettyString(SignalRecord record, int maxScanningDays) {
        StringBuilder sb = new StringBuilder();
        DecimalFormat format = new DecimalFormat("#0.0000");
        DecimalFormat shortFormat = new DecimalFormat("#0.00");
        sb.append(fillRight(record.getStrategyName(), " ", 40));
        sb.append(" | ");
        sb.append(fillCenter(format.format(record.getStrategyGain()) + " / " + format.format(record.getBuyAndHoldGain()) + " : " + shortFormat.format(record.getAvgTransTime()), " ", 35));
        sb.append(" | ");
        sb.append(fillLeft(record.getDateTime().format(dateTimeFormatter), " ", 15));
        sb.append(" | ");
        sb.append(fillLeft(record.getSignal(), " ", 5));
        sb.append(" | ");
        sb.append(fillRight(generateBar(record, maxScanningDays, 50), " ", 59) + "|");
        sb.append((record.getStrategyClassName() != "") ? "  ::  " + record.getStrategyClassName() : "");
        return sb.toString();
    }

    public static String prettyString(AnalysisRecord data) {
        StringBuilder sb = new StringBuilder();
        DecimalFormat format = new DecimalFormat("###0.0000");
        sb.append(fillLeft(data.getSource(), " ", 60));
        sb.append(" | ");
        sb.append(fillCenter(data.getMarketState(), " ", 18));
        sb.append(" | ");
        sb.append(fillLeft("value = " + fillLeft(format.format(data.getValue()), " ", 9), " ", 20));
        sb.append(" | ");
        sb.append(data.getDescription());
        return sb.toString();
    }


}
