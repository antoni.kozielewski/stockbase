package org.hou.stocktank.platform.stockBase.downloader;

import org.hou.stocktank.platform.stockBase.events.StockBaseEvent;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.Map;

import static org.hou.stocktank.platform.stockBase.downloader.FileDownloaderEvent.DATA_FILEKEY;
import static org.hou.stocktank.platform.stockBase.downloader.FileDownloaderEvent.DATA_FILEPATH;
import static org.hou.stocktank.platform.stockBase.downloader.FileDownloaderEvent.DATA_VALUE_DOWNLOAD_FINISHED;

@ShellComponent
@ConfigurationProperties("downloader")
public class StockBaseFileDownloaderComponent {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(StockBaseFileDownloaderComponent.class);
    public static final String ID = "DOWNLOADER";
    private Map<String, String> files;
    private String filesDir;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Async
    @EventListener(classes = StockBaseEvent.class)
    public void execute(Object eventSrc) {
        StockBaseEvent event = (StockBaseEvent) eventSrc;
        if (event.getTarget().equals(ID) && event.getType().equals(StockBaseEvent.Type.EXECUTE)) {
            String fileKey = event.getValue();
            if (files.containsKey(fileKey)) {
                logger.info("start downloading file : " + fileKey );
                String filePath = download(fileKey);
                if (filePath != "") {
                    logger.info("file downloaded : " + filePath );
                    notifySuccess(fileKey, filePath);
                }  else {
                    logger.error("file downloaded failed : " + fileKey);
                }
            } else {
                logger.error("Timer event to download: {} but it's not defined in configuration.", fileKey);
            }
        }
    }

    private void notifySuccess(String fileKey, String filePath) {
        FileDownloaderEvent event = new FileDownloaderEvent(this, StockBaseEvent.ALL, StockBaseEvent.Type.NOTIFICATION, DATA_VALUE_DOWNLOAD_FINISHED);
        event.putData(DATA_FILEKEY, fileKey);
        event.putData(DATA_FILEPATH, filePath);
        applicationEventPublisher.publishEvent(event);
    }

    private String getCorrectFilesDir() {
        return (filesDir.endsWith(File.separator)) ? filesDir : filesDir + File.separator;
    }

    private String getFileExtension(String url) {
        if (url.contains(".") && url.lastIndexOf(".") >= url.length() - 4) {
            return url.substring(url.lastIndexOf("."), url.length());
        }
        return "";
    }

    private String download(String fileKey) {
        if (files.containsKey(fileKey)) {
            String url = files.get(fileKey);
            ReadableByteChannel in = null;
            try {
                logger.debug("downloading : {}", url);
                in = Channels.newChannel(new URL(url).openStream());
                String filePath = getCorrectFilesDir() + fileKey + getFileExtension(url);
                FileChannel out = new FileOutputStream(filePath).getChannel();
                out.transferFrom(in, 0, Long.MAX_VALUE);
                out.close();
                logger.debug("download finished : {} | filePath: {}", url, filePath);
                return filePath;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            logger.error("There is no file-key: {} in current configuration.", fileKey);
            return "";
        }
    }

    @ShellMethod("file downloader management")
    public void downloader(@ShellOption(defaultValue = "") String command) {
        if (command.equals("config")) {
            System.out.println("Downloader config: ");
            System.out.println("    filesDir = " + filesDir);
            System.out.println("    files: ");
            files.entrySet().stream().forEach(entry -> System.out.println("         " + entry.getKey() + " : " + entry.getValue()));
        } else {
            System.out.println("Downloader commands: ");
            System.out.println("         'downloader config' - shows current configuration");
        }
    }

    public Map<String, String> getFiles() {
        return files;
    }

    public void setFiles(Map<String, String> files) {
        this.files = files;
    }

    public String getFilesDir() {
        return filesDir;
    }

    public void setFilesDir(String filesDir) {
        this.filesDir = filesDir;
    }
}