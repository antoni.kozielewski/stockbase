package org.hou.stocktank.platform.stockBase.stocktank.reports.txt;


import org.hou.stocktank.platform.stockBase.stocktank.storage.AnalysisRecord;
import org.hou.stocktank.platform.stockBase.stocktank.storage.SignalRecord;

import java.text.DecimalFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ReportTXTUtil {
    static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE;

    private ReportTXTUtil() {
        super();
    }

    public static String fillRight(String s, String fill, int length) {
        String result = s;
        for (int i = result.length(); i < length; i++) {
            result = result + fill;
        }
        return result;
    }

    public static String fillLeft(String s, String fill, int length) {
        String result = s;
        for (int i = result.length(); i < length; i++) {
            result = fill + result;
        }
        return result;
    }

    public static String fillCenter(String s, String fill, int length) {
        return fillLeft(fillRight(s, fill, s.length() + (length - s.length()) / 2), fill, length);
    }

    static int calculateBar(long value, int maxInput, int maxOutput) {
        return maxOutput - (int) (Math.log10(maxInput - value + 1) * (maxOutput / Math.log10(maxInput)));
    }

    public static String getBarString(long value, int max, int length, String fill) {
        return fillLeft("" + (max - value), " ", 3) + " " + fillLeft("", fill, calculateBar(value, max, length));
    }

    public static String progessBar(int max, int percentValue) {
        return fillRight("\r|" + fillLeft("", "#", percentValue * max / 100), " ", max) + "|  " + percentValue + "%";
    }

    public static String createFrame(String... content) {
        int width = 0;
        for (String s : content) {
            if (width < s.length()) {
                width = s.length();
            }
        }
        width = width + 4;
        String result = "|" + fillLeft("", "-", width) + "|\n\r";
        for (String s : content) {
            result += "|" + fillRight(" " + s + " ", " ", width) + "|\n\r";
        }
        result += "|" + fillRight("", "-", width) + "|\n\r";
        return result;
    }

    public static String generateBar(SignalRecord signalRecord, int max, int length) {
        return getBarString(max - (Duration.between(signalRecord.getDateTime(), ZonedDateTime.now()).getSeconds() / (3600 * 24)), max, length, "-") + ">";
    }

    public static String prettyString(SignalRecord record, int maxScanningDays) {
        StringBuilder sb = new StringBuilder();
        DecimalFormat format = new DecimalFormat("#0.0000");
        DecimalFormat shortFormat = new DecimalFormat("#0.00");
        sb.append(fillRight(record.getStrategyName(), " ", 40));
        sb.append(" | ");
        sb.append(fillCenter(format.format(record.getStrategyGain()) + " / " + format.format(record.getBuyAndHoldGain()) + " : " + shortFormat.format(record.getAvgTransTime()), " ", 35));
        sb.append(" | ");
        sb.append(fillLeft(record.getDateTime().format(dateTimeFormatter), " ", 15));
        sb.append(" | ");
        sb.append(fillLeft(record.getSignal(), " ", 5));
        sb.append(" | ");
        sb.append(fillRight(generateBar(record, maxScanningDays, 50), " ", 59) + "|");
        sb.append((record.getStrategyClassName() != "") ? "  ::  " + record.getStrategyClassName() : "");
        return sb.toString();
    }

    public static String prettyString(AnalysisRecord data) {
        StringBuilder sb = new StringBuilder();
        DecimalFormat format = new DecimalFormat("###0.0000");
        sb.append(fillLeft(data.getSource(), " ", 60));
        sb.append(" | ");
        sb.append(fillCenter(data.getMarketState(), " ", 18));
        sb.append(" | ");
        sb.append(fillLeft("value = " + fillLeft(format.format(data.getValue()), " ", 9), " ", 20));
        sb.append(" | ");
        sb.append(data.getDescription());
        return sb.toString();
    }


}
